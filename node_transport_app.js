var config = require('./config.json');
var constants = require('./constants');
var arr_socket_clients = [];

var log4js = require('log4js');
var redis = require("redis");
var webSocket = require("ws");
var http = require("http");
var request = require('request');
var zlib = require('zlib');

/* Logger ----------------------------------------------------------------------------------------------------------- */
log4js.configure({
    appenders: [
        { "type": 'console' },
        {
            "type": 'file',
            "filename": process.env.NODE_ENV == 'local' ? 'logs/log_file.log' : '/var/lib/jenkins/.pm2/logs/log_file.log',
            "maxLogSize": 1045876,
            "backups": 100,
            "category": "absolute-logger"
        }
    ]
});

var logger = log4js.getLogger("absolute-logger");
logger.trace(' start NODE_TRANSPORT_APP 1');
logger.trace(process.env.NODE_ENV);



/*SOCKET SERVER*/
var wss = new webSocket.Server({ port: 60100 });


/* REDIS ------------------------------------------------------------------------------------------------------------ */
var config_redis;
if(process.env.NODE_ENV_COMPUTER == 'and'){
    logger.trace(' redis and');
    config_redis = {
        port  : config.redis_port
    };
}else if (process.env.NODE_ENV_COMPUTER == 'dim'){
    logger.trace('redis dim');
    config_redis = {
        port  : config.redis_port,
        host: '10.11.43.15'
    };
}else{
    logger.trace(' redis test server port:'+config.redis_port);
    config_redis = {
        port  : config.redis_port
    };
}


var client = redis.createClient(config_redis);
var client_2 = client.duplicate();

client.on("error", function (err) {
    logger.error(err);
});

client.on("message", function(channel, message){

    if(channel == 'play'){

        var redis_mes = JSON.parse(message);
        var chat_id = redis_mes['chat_id'];
        var game_id = redis_mes['game_id'];

        logger.trace(" Play:" + channel  + message );

        if(chat_id && game_id){

            logger.trace(" Check soket array");

            if (arr_socket_clients[chat_id]) {
                logger.trace(' SOKKET IN ARRAY');
                ws_request(redis_mes, arr_socket_clients[chat_id]);
            } else {
                logger.trace(' CREATE SOKKET');
                createWs(redis_mes);
            }

        }else{
            logger.error(' NOT SET chat_id and game_id');
        }
    }

    if(channel == 'cleaner'){

        var arr_chat_id = JSON.parse(message);

        logger.trace(" Cleaner: chanel-" + channel  + ", message-" + message );
        logger.trace(" Arr_chat_id: " + arr_chat_id );

        if(arr_chat_id && arr_chat_id.length > 0){

            logger.trace(" if (arr_chat_id && arr_chat_id.length > 0) - true");
            logger.trace(" arr_chat_id.length:"+arr_chat_id.length);



            /*arr_socket_clients.forEach(function (value, index) {

                logger.trace(" Arr_socket_clients: index " + index );
                logger.trace(" Arr_socket_clients: value " + value._socket._handle.fd );

            });*/

            arr_chat_id.forEach(function (chat_id) {

                logger.trace(" ForEach chat_id: " + chat_id );

                if (ws_close = arr_socket_clients[chat_id]) {

                    logger.trace(" ws_close: " + ws_close._socket._handle.fd );

                    ws_close.close();
                }

            });

            for (var key in arr_socket_clients)
            {
                logger.trace(" Key: index " + key );

            }
        }
    }
});

client.subscribe("play");
client.subscribe("cleaner");


/* RESET USER_CONDITION CONNECTED ----------------------------------------------------------------------------------- */
client_2.lrange("redis_user_condition", 0, -1, function(err, reply) {

    if(reply && reply.length > 0){

        reply.forEach(function (redis_user_condition) {

            client_2.hmset(
                "redis_user_condition:a:"+redis_user_condition,
                [
                    'update_date', (Date.now() /1000 |0),
                    'provider_user_id', 0,
                    'table_id', 0,
                    'session_id', 0,
                    "connected", 0,
                    'playing', 0,
                    "bet_accepted", 0
                ],
                function (err, res) {
                    logger.trace(' SET CONNECTED:0  FOR: ' + redis_user_condition);
                }
            );
        });

    }
});


/* FUNCTION createWs ------------------------------------------------------------------------------------------------ */
var createWs = function (redis_mes) {

    logger.trace(' Start create WS , ws_address:'+config.ws_address);

    arr_socket_clients[redis_mes['chat_id']] = new webSocket(config.ws_address);

    var ws = arr_socket_clients[redis_mes['chat_id']];

    logger.trace(' End create WS , ws_address:'+config.ws_address);


    ws.on("open", function open() {
        logger.trace(" Connected soket_id:" + redis_mes['chat_id']);
        //arr_socket_clients[redis_mes['chat_id']] = ws;

        var ws_open_user_id =  {"ws_open_user_id": redis_mes['chat_id'] };
        wss.broadcast( JSON.stringify(ws_open_user_id));

        logger.trace(' WS_ID:'+ws._socket._handle.fd);

        ws_request(redis_mes, ws);
    });

    ws.on("message", function (data) {

        var buffer = Buffer(data);

        //logger.trace(buffer);

        if(buffer && buffer.length>4){

            var message_length = buffer.slice(0, 4).readIntBE(0, 4).toString();
            var buf_slice = buffer.slice(4, buffer.length);

            var zlib_inflate =  zlib.inflateSync(buf_slice);

            var user_provider_id = zlib_inflate.slice(0, 4);
            var command = zlib_inflate.slice(4, 5);
            var data_json_length = zlib_inflate.slice(5, 9);
            var data_json = zlib_inflate.slice(9, zlib_inflate.length);

            logger.trace(' on.message - USER_ID:'+user_provider_id.readIntBE(0, 4).toString()+', COMMAND:'+command.readInt8().toString()+', DATA:'+data_json.toString('utf8'));


            switch (parseInt(command.readInt8().toString())) {

                case constants.MULTIPLAYER_PLAYER_ADD:
                    logger.trace(' 100 MULTIPLAYER_PLAYER_ADD: on');

                    client_2.hgetall("redis_user_condition:a:"+redis_mes['chat_id'], function(err, reply) {

                        logger.trace(' 100 MULTIPLAYER_PLAYER_ADD: connected:'+reply.hasOwnProperty('connected'));
                        logger.trace(' 100 MULTIPLAYER_PLAYER_ADD: connected:'+reply.connected);

                        if(!reply.hasOwnProperty('connected') || reply.connected == 0){
                            client_2.hmset("redis_user_condition:a:"+redis_mes['chat_id'], ['update_date', (Date.now() /1000 |0), "connected", 1, 'provider_user_id', user_provider_id.readIntBE(0, 4).toString()], function (err, res) {

                                logger.trace(' 100 MULTIPLAYER_PLAYER_ADD: SET connected:1');

                                //ws_request(redis_mes, ws);
                            });
                        }
                    });
                    break;

                case constants.AUTOROLL_MAKE_BET_TIMER:
                    logger.trace(' 101 AUTOROLL_MAKE_BET_TIMER: on');
                    logger.trace(' 101 AUTOROLL_MAKE_BET_TIMER: ws:'+ws._socket._handle.fd);
                    logger.trace(' 101 AUTOROLL_MAKE_BET_TIMER: ws[chat_id]-'+redis_mes['chat_id']);
                    ws.bet_timer = true;
                    client_2.hgetall("redis_user_condition:a:"+redis_mes['chat_id'], function(err, reply) {
                        if(reply.playing == 1 && reply.bet_accepted == 0 && reply.connected == 1){
                            logger.trace(' 101 AUTOROLL_MAKE_BET_TIMER: WS_REQUEST');
                            ws_request(redis_mes, ws);
                        }
                    });
                    break;

                case constants.AUTOROLL_MADE_BET_TIMER:
                    logger.trace(' 102 AUTOROLL_MADE_BET_TIMER: on');
                    ws.bet_timer = false;
                    break;

                case constants.MULTIPLAYER_ROLL_DICE:
                    logger.trace(' 103 MULTIPLAYER_ROLL_DICE: on');
                    var bet_accepted = JSON.parse(data_json.toString('utf8'));
                    if(bet_accepted['made']){
                        client_2.hgetall("redis_user_condition:a:"+redis_mes['chat_id'], function(err, reply) {
                            client_2.hmset("redis_user_condition:a:"+redis_mes['chat_id'], ['update_date', (Date.now() /1000 |0), "bet_accepted", 1], function (err, res) {
                                logger.trace(' 103 MULTIPLAYER_ROLL_DICE: SET bet_accepted:1');
                            });
                        });
                    }
                    else if(bet_accepted['made'] == false && bet_accepted['ErrorCode'] == 1001){
                        logger.trace(' 103 MULTIPLAYER_ROLL_DICE: ERROR 1001: PLAER NO_MANY');
                        yii2_adapter_send_result(redis_mes['chat_id'], JSON.stringify({"error":true, "code": bet_accepted['ErrorCode']}));
                        ws.close();
                    }
                    else if(bet_accepted['made'] == false && bet_accepted['ErrorCode'] != ""){
                        logger.trace(' 103 MULTIPLAYER_ROLL_DICE: ERROR OTHER: '+ bet_accepted['ErrorCode']);
                        client_2.hgetall("redis_user_condition:a:"+redis_mes['chat_id'], function(err, reply) {
                            client_2.hmset("redis_user_condition:a:"+redis_mes['chat_id'], ['update_date', (Date.now() /1000 |0), "bet_accepted", 0, 'playing', 0], function (err, res) {
                                yii2_adapter_send_result(redis_mes['chat_id'], JSON.stringify({"error":true, "code": bet_accepted['ErrorCode']}));
                            });
                        });

                    }
                    break;

                case constants.MULTIPLAYER_CASINO_DICE:
                    logger.trace(' 104 MULTIPLAYER_CASINO_DICE: on');
                    client_2.hgetall("redis_user_condition:a:"+redis_mes['chat_id'], function(err, reply) {
                        client_2.hmset("redis_user_condition:a:"+redis_mes['chat_id'], ['update_date', (Date.now() /1000 |0), "bet_accepted", 0, 'playing', 0], function (err, res) {

                            logger.trace(' 104 MULTIPLAYER_CASINO_DICE: SET bet_accepted:0, playing:0');
                            yii2_adapter_send_result(redis_mes['chat_id'], data_json.toString('utf8'));
                        });
                    });
                    break;

                case constants.MULTIPLAYER_ALL_BETS_INFO:
                    logger.trace(' 107 MULTIPLAYER_ALL_BETS_INFO: on');
                    logger.info(" Info bets: "+command.readInt8().toString()+ ' - '+ constants[command.readInt8().toString()]);
                    break;

                default:
                    yii2_adapter_send_result(redis_mes['chat_id'], JSON.stringify({"error":true, "code":command.readInt8().toString()}));
                    logger.error(" RESPONSE STATUS: "+command.readInt8().toString()+ ' - '+ constants[command.readInt8().toString()]);
                    ws.close();
            }
        }

    });

    ws.on('close', function close() {
        logger.info(" Closed soket_id:" + redis_mes['chat_id']);
        delete  arr_socket_clients[redis_mes['chat_id']];
        //arr_socket_clients.splice(redis_mes['chat_id'], 1);

        var ws_close_user_id =  {"ws_close_user_id": redis_mes['chat_id'] };
        wss.broadcast( JSON.stringify(ws_close_user_id));

        client_2.hgetall("redis_user_condition:a:"+redis_mes['chat_id'], function(err, reply) {

            if(reply){
                client_2.hmset(
                    "redis_user_condition:a:"+redis_mes['chat_id'],
                    [
                        'update_date', (Date.now() /1000 |0),
                        'provider_user_id', 0,
                        'table_id', 0,
                        'session_id', 0,
                        "connected", 0,
                        'playing', 0,
                        "bet_accepted", 0
                    ],
                    function (err, res) {
                        logger.trace(' UNSET USER_CONDITION:' + redis_mes['chat_id']);
                    }
                );
            }
        });
    });

    ws.on('error', function (err) {
        logger.error(" Error soket_id:" + redis_mes['chat_id'] + " - " + err);
        ws.close();
    });

};


/* FUNCTION ws_request ---------------------------------------------------------------------------------------------- */
var ws_request = function (redis_mes, ws) {

    logger.info(' START ws_request function');

    client_2.hgetall("redis_user_condition:a:"+redis_mes['chat_id'], function(err, reply) {

        if(err){
            logger.error(err);
            yii2_adapter_send_result(redis_mes['chat_id'], JSON.stringify({"error":true, "code":7}));
        }else{

            //logger.info(reply);

            if(reply && check_redis_dates(reply, redis_mes)){

                var game_connected = reply.hasOwnProperty('connected');

                if(!game_connected || (parseInt(reply.connected ,10)=== 0 || reply.connected === 'undefined')){

                    logger.info(' Start connected game ' +reply.connected);

                    var hash = reply.session_id;
                    var roomId =  parseInt(reply.table_id, 10);

                    var buf1 =  Buffer.alloc(4);
                    buf1.writeIntBE(0);

                    var buf2 =  Buffer.alloc(1);
                    buf2.writeInt8(100);

                    var data_json = JSON.stringify({'hash':hash, 'roomId':roomId});
                    var buf3 =  Buffer.from(data_json);

                    var buf4 =  Buffer.alloc(4);
                    buf4.writeIntBE(Object.keys(data_json).length, 0, 4);

                    var buf_message  =  Buffer.concat([buf1, buf2, buf4, buf3 ]);
                    var zlib_message =  zlib.deflateSync(buf_message);

                    var totalLength = zlib_message.length;

                    var buf5 =  Buffer.allocUnsafe(4);
                    buf5.writeIntBE(totalLength, 0, 4);

                    var bufAll = Buffer.concat([buf5, zlib_message]);

                    var zlib_finish = zlib.deflateSync(bufAll);

                    //logger.info(bufAll);
                    logger.info(' WS_SEND( USERID:0, COMMAND:100, DATA:' + data_json+ ')');

                    ws.send(bufAll, {binary: true});

                }else{

                    if(ws.bet_timer){

                        logger.info(' Throw DICE ' +reply.connected);

                        var user_id =  Buffer.alloc(4);
                        user_id.writeIntBE(parseInt(reply.provider_user_id, 10), 0, 4);

                        var command =  Buffer.alloc(1);
                        command.writeInt8(103);

                        var params_json = JSON.parse(reply.params);
                        var gameDescription = params_json.games[redis_mes['game_id']];

                        var data_json_play = JSON.stringify(gameDescription);
                        var data =  Buffer.from(data_json_play);

                        var l_data_json_play =  Buffer.alloc(4);
                        l_data_json_play.writeIntBE(Object.keys(data_json_play).length, 0, 4);

                        var b_message  =  Buffer.concat([user_id, command, l_data_json_play, data ]);
                        var zlib_message_1 =  zlib.deflateSync(b_message);

                        var totalLength_1 = zlib_message_1.length;

                        var buf7 =  Buffer.allocUnsafe(4);
                        buf7.writeIntBE(totalLength_1, 0, 4);

                        var bufAll_1 = Buffer.concat([buf7, zlib_message_1]);

                        if(ws.bet_timer){
                            ws.send(bufAll_1, { binary: true});
                            logger.info(' WS_SEND( USERID:'+parseInt(reply.provider_user_id, 10)+' COMMAND:103, DATA:' + data_json_play+')');
                        }
                    }
                }

            }else{
                logger.error(' Not find redis dates');
                yii2_adapter_send_result(redis_mes['chat_id'], JSON.stringify({"error":true, "code":7}));
            }

        }

    });
};

/* FUNCTION yii2_adapter_send_result -------------------------------------------------------------------------------- */
var yii2_adapter_send_result = function (chat_id, data) {

    /*if(process.env.NODE_ENV == 'local') {
        if (process.env.NODE_ENV_COMPUTER == 'dim') {
            hostname = 'adapter.dim';
        }
    }else{
        hostname = 'tele.gramm.co';
    }*/


    /*var hostname = config.host_adapter;
    logger.info(' HOST_ADAPTER: ' + hostname);

    var options = {
        hostname: hostname,
        path: '/adapter/game-result',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    var req = http.request(options, function(res) {
        console.log(res);

        logger.info(' HOST: ' + hostname);
        logger.info(' HTTP send result - Status: ' + res.statusCode);
    });
    req.on('error', function(e) {
        logger.error(' ERROR HTTP send result: '+e.message);
    });

    req.write('{"chat_id":'+chat_id+', "data":'+ data+ '}');
    req.end();*/

    var url_adapter = 'https://'+config.host_adapter+'//adapter/game-result';

    request.post({
        headers: {'content-type' : 'application/json'},
        url:     url_adapter,
        body:    '{"chat_id":'+chat_id+', "data":'+ data+ '}'
    }, function(error, response, body){

        if(error){
            logger.error(' ERROR HTTP send result');
            console.log(error);
        }else{
            logger.info(' HOST: ' + url_adapter);
            logger.info(' HTTP send result - Status: ' + response.statusCode);
        }
    });
};


/* FUNCTION check_redis_dates --------------------------------------------------------------------------------------- */
var check_redis_dates = function (reply, redis_mes) {

    var params_json = reply.params ? JSON.parse(reply.params) : 'undefined';

    var gameDescription = (params_json.hasOwnProperty('games') && params_json.games[redis_mes['game_id']] !== 'undefined') ? params_json.games[redis_mes['game_id']] : false;

    return (reply.session_id && reply.table_id && gameDescription) ? true : false;

};


function heartbeat() {
    this.isAlive = true;
}

/*SOCKET SERVER*/
wss.on('connection', function connection(ws) {

    ws.isAlive = true;
    ws.on('pong', heartbeat);

    //arr_socket_clients['12434234']='fdg';
    //arr_socket_clients['12434235']='fdg';

    var ws_array_user_id = arr_socket_clients.length > 0 ? Object.keys(arr_socket_clients) : null;

    var m = {"ws_array_user_id": ws_array_user_id };

    logger.info(' Object.keys(arr_socket_clients): ' + JSON.stringify(m));

    ws.send(JSON.stringify(m));
});

wss.on('close', function connection(ws) {

    var ip = ws.upgradeReq.connection.remoteAddress;
    logger.info(' Close: ' + ip);

});

// Broadcast to all.
wss.broadcast = function broadcast(data) {
    wss.clients.forEach(function each(client) {
        if (client.readyState === webSocket.OPEN) {
            client.send(data);
        }
    });
};

const interval = setInterval(function ping() {
    wss.clients.forEach(function each(ws) {
        if (ws.isAlive === false) return ws.terminate();

        ws.isAlive = false;
        ws.ping('', false, true);
    });
}, 30000);