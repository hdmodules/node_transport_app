var assert = require('assert');
var redis = require("redis");
var config = require('./../config.json');

var config_redis = { port  : config.redis_port };

var client = redis.createClient(config_redis);

describe('CREATE REDIS CLIENT {port:'+config.redis_port+'}', function() {

    /*beforeEach(function(){});*/

    it('Connected', function() {
        assert(client.connected);

    });

    it('Connected end', function() {
        if(client.connected){
            client.end(true);
            assert(!client.connected);
        }else {
            assert(false);
        }
    });

    /*after(function() {});*/
    assert(true);
});