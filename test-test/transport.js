const assert = require('assert');
const redis = require("redis");
const config = require('./../config.json');

const config_redis = { port  : config.redis_port };
const sub = redis.createClient(config_redis);
const pub = redis.createClient(config_redis);

var callFuncAfter = function (func, max) {
    var i = 0;
    return function (err) {
        if (err) {
            throw err;
        }
        i++;
        if (i >= max) {
            func();
            return true;
        }
        return false;
    };
};

var channel = 'bet';
var message = 'test';
var messages = ['test message 1', 'test message 2'];


describe('REDIS PUB_SUB', function() {


    it('Connected sub client_1', function() {
        assert(sub.connected);
    });

    it('Connected pub client_2', function() {
        assert(pub.connected);
    });

    it('Receives messages on subscribed channel', function (done) {

        var end = callFuncAfter(done, 200);

        sub.on('subscribe', function (chnl, count) {
            for (var i = 0; i < 100; i++) {
                console.log(i);
                pub.publish(channel, message, function (err, res) {
                    console.log('pub');
                    end();
                });
            }

        });

        sub.on('message', function (chnl, msg) {

            assert.equal(chnl, channel);
            assert.equal(msg, 'test');
            console.log('mm');
            end();
        });

        sub.subscribe(channel);
    });

    it('Connected pub end', function() {
        if(pub.connected){
            pub.end(true);
            assert(!pub.connected);
        }else {
            assert(false);
        }
    });

    it('Connected sub end', function() {
        if(sub.connected){
            sub.end(true);
            assert(!sub.connected);
        }else {
            assert(false);
        }
    });

});



