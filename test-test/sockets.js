const webSocket = require("ws");
const assert = require('assert');
const config = require('./../config.json');

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Create WebSocketServer --------------------------------------------------------------------------------------------//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var WebSocketServer = webSocket.Server;
var wss = new WebSocketServer({ port: 8000 });

describe('WEB_SOCKET_SERVER CREATE', function() {
    it('Create', function() {
        assert(wss);
    });
});

wss.on('connection', function connection(ws) {

    var id = ws.upgradeReq.headers['sec-websocket-key'];

    describe('WEB_SOCKET_SERVER open connection', function() {
        it('Open connection for:'+id, function(done) {
            assert(ws);
            done();
        });
    });


    ws.on('message', function incoming(message) {
        describe('WEB_SOCKET_SERVER received message', function() {
            it('Received message:' + message, function(done) {
                done();
            });
        });
    });

    ws.send('//response WS_server message//');

});

wss.on('error', function (error) {
    describe('WEB_SOCKET_SERVER error', function() {
        it('Error', function(done) {
            done();
        });

    });
});


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Create WebSocketClient --------------------------------------------------------------------------------------------//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var client = new webSocket(config.ws_address);

describe('CLIENT WEBSOCKET CREATE', function() {
    it('Create', function() {
        assert(client);
    });
});

client.on("open", function open() {
    client.send("//response WS_client message//");
    describe('CLIENT WEBSOCKET OPEN AND SENT MESSAGE', function() {

        it('Open', function(done) {
            assert(client.readyState);
            done();
        });

    });
});

client.on("message", function (data) {
    describe('CLIENT WEBSOCKET RECIVED MESSAGE', function() {
        it('Received message:' + data, function(done) {
            client.close();
            assert(data);
            done();
        });
    });
});

client.on('close', function close() {
    describe('CLIENT WEBSOCKET CLOSE', function() {
        it('Close', function(done) {
            done();
        });

    });
});

client.on('error', function (err) {
    describe('CLIENT WEBSOCKET ERROR', function() {
        it('Close', function(done) {
            done();
        });

    });
});




