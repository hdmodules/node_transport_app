const assert = require('assert');
const redis = require("redis");
const config = require('./../config.json');

const config_redis = { port  : config.redis_port };
const sub = redis.createClient(config_redis);
const pub = redis.createClient(config_redis);


var channel = 'bet';
var messages = ['test message 1', 'test message 2'];

describe('REDIS PUB_SUB', function() {

    it('Connected sub client_1', function() {
        assert(sub.connected);
    });

    it('Connected pub client_2', function() {
        assert(pub.connected);
    });


    sub.on('subscribe', function (chnl, count) {
        describe('Publish messages (channel '+chnl+')', function() {
            messages.forEach(function (mss) {
                it('pub :' + mss, function(done) {
                    pub.publish(channel, mss);
                    done();
                });
            });

        });
    });

    sub.on('message', function (chnl, msg) {

        describe('Subscriber received message' , function() {
            it(msg, function(done) {
                assert.equal(chnl, channel);
                done();
            });
        });

    });

    sub.subscribe(channel);

});






