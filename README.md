# NODE_TRANSPORT_APP

[![N|Solid](http://flexiapps.net/eng/images/techs/express-js.jpg)](http://expressjs.com/)

NODE_TRANSPORT_APP iis a service that serves the requests between the gaming provider and telegram-adapter

### Tech

Dillinger uses a number of open source projects to work properly:

* [node.js] - evented I/O for the backend
* [Express] - fast node.js network app framework
* [redis] - Pub/Sub [Publish–subscribe pattern]
* [ws] - node.js websocket library


### Todos

 - Write Tests
 - Rethink Github Save
 - Add Code Comments
 - Add Night Mode


**Free Software, Hell Yeah!**

   [node.js]: <http://nodejs.org>
   [Express]: <http://expressjs.com>
   [Redis]: <http://redis.io/>
   [Publish–subscribe pattern]: <https://en.wikipedia.org/wiki/ublish–subscribe_pattern>
   [ws]: <https://github.com/websockets/ws>