const config = require('./../config.json');
const assert = require('assert');
const log4js = require('log4js');
const querystring = require('querystring');
const crypto = require('crypto');
const get = require('simple-get');
//const md5 = require('md5');
//var ksort = require('ksort');

// Logger --------------------------------------------------------------------------------------------------------------
log4js.configure({
    appenders: [
        { "type": 'console' },
        {
            "type": 'file',
            "filename": 'logs/log_file_test.log',
            "maxLogSize": 20480,
            "backups": 10,
            "category": "absolute-logger"
        }
    ]
});

var logger = log4js.getLogger("absolute-logger");
logger.trace('start RUN_GAME_TEST');
logger.trace('NODE_ENV:'+process.env.NODE_ENV);


describe('Send request', function() {
    it('Start game', function(done) {

        var params = {};
        var url = 'http://api.dev.table10.org/api.php/external/start?';

        params['account'] =  '247';
        params['balance'] = 1000;
        params['currency'] = 'USD';
        params['gameType'] = 'dice';
        params['lang'] = 'en';
        params['login'] = 'test_telegram_adapter';
        params['password'] =  'VwxRy9u0Lssr';
        params['username'] = 'test_telegram_adapter';

        var q = querystring.stringify(params);
        var sign = crypto.createHash('md5').update(q).digest('hex');

        delete params['password'];
        params['sign'] =  sign;

        var req = url+querystring.stringify(params);

        get(req, function (err, res) {

            if(err){
                logger.info("Error :"+ err);
                done();
            }
            else{

                logger.info(req);
                logger.info("Response code:"+ res.statusCode);

                res.setEncoding('utf8');
                res.on('data', function(chunk){
                    console.log(chunk);
                    assert(res.statusCode == '200');
                    done();
                });

            }
        });

    });
});

